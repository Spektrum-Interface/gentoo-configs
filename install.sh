#!/bin/sh
if [ -e /usr/bindoas]
then
  _EXCAL=doas
else
  _EXCAL=sudo
echo "installing the directory..\n"
echo "backing up old portage dir.."
$_EXCAL mv -v /etc/portage /etc/portage-bak
echo "setting the portage dir"
$_EXCAL mv -v portage /etc/portage
echo "since i'm using git to sync, I am gonna update also /var/db/repo/gentoo"
[-d /var/db/repos/gentoo ] && mv /var/db/repos/gentoo /var/db/repos/gentoo-bak
$_EXCAL emerge --sync

