#!/bin/sh

#die function in case something goes wrong 
die (){
 echo ${1}
 exit 1
 rm -r /tmp/pipewire-libudev-zero > /dev/null 
}

_TMPDIR=/tmp # /tmp should be a good "sandbox"
#checking if git is there. as a safety measure

if which git >/dev/null
then
 _GITEXEC=$(which git)
else
 die "git does not exists. "
fi
cd $_TMPDIR # changing to /tmp so we have a isolated dir

#cloning capezotte's aur repo
$_GITEXEC clone https://aur.archlinux.org/pipewire-libudev-zero.git || die "failed to clone git repository"

#I do this more as a safety measure than anything really
echo "removing previous patch as a security measure.."
rm /etc/portage/patches/media-video/pipewire/luz.patch >/dev/null

#well, the echo explain what this is doesn't it
echo "moving patch to the portage dir.."
cp /tmp/pipewire-libudev-zero/luz.patch /etc/portage/patches/media-video/pipewire/luz.patch || die "failed to move the patch"

#see line 28
echo "cleaning up.."
rm -r /tmp/pipewire-libudev-zero > /dev/null || die "failed to clean up" 
